import { Component, OnInit } from '@angular/core';
import { ApiModel } from '../common/models/apilist.model';
import { ApiServicesService } from '../common/services/api-services.service'
import { Observable } from 'rxjs';
import { Routes, Router, ActivatedRoute } from '@angular/router';



@Component({
  selector: 'app-api-service',
  templateUrl: './api-service.component.html',
  styleUrls: ['./api-service.component.css']
})
export class ApiServiceComponent implements OnInit {

  apilist: ApiModel[];
  //ApiData: string = "Kindly select an API from the list";
  ApiData
  
 // ApiData: Observable;
  //apiUrls = './assets/data/links.json'

  constructor(private ApiServicesService:ApiServicesService, private router:Router, private  route: ActivatedRoute) { }

  ngOnInit() {
   this.ApiServicesService.getApiList().subscribe((apilist: ApiModel[]) =>  {
      this.apilist = apilist
      console.log(apilist)
      //this.apilist.name = data.title;
     // this.apilist.link = data.link;
  })
 
  }
  onSelecApi(URL, i){
    //alert(URL +  "," + i)
    //this.selectedApi = true
    this.ApiData = "Loading..."
    var k;
     for (k = 0; k < document.querySelectorAll('.selected').length; k++) {
       document.querySelectorAll('.selected')[k].classList.remove('selected');
     }
    document.getElementsByClassName('list-group-item')[i].classList.add('selected')

    //this.classList.add = 'selected'
    this.ApiServicesService.loadApiData(URL).subscribe(ApiDataRes =>  {
      //this.router.navigate(['./api-detail', i], { relativeTo: this.route });
      console.log(this.ApiData, localStorage.token_type)
      console.log('ACCESS_TOKEN', localStorage.access_token)
      this.ApiData = JSON.stringify(ApiDataRes, undefined, 4);
    },
    err => this.ApiData = err.status + " - " + err.statusText,
    
    )
  }

  syntaxHighlight(json) {
    if (typeof json != 'string') {
         json = JSON.stringify(json, undefined, 2);
    }
    json = json.replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;');
    return json.replace(/("(\\u[a-zA-Z0-9]{4}|\\[^u]|[^\\"])*"(\s*:)?|\b(true|false|null)\b|-?\d+(?:\.\d*)?(?:[eE][+\-]?\d+)?)/g, function (match) {
        var cls = 'number';
        if (/^"/.test(match)) {
            if (/:$/.test(match)) {
                cls = 'key';
            } else {
                cls = 'string';
            }
        } else if (/true|false/.test(match)) {
            cls = 'boolean';
        } else if (/null/.test(match)) {
            cls = 'null';
        }
        return '<span class="' + cls + '">' + match + '</span>';
    });
}

}
