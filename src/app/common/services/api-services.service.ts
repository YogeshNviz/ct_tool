import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ApiServicesService {

  apiUrls = './assets/data/links.json'
  details = '';

  constructor(private http: HttpClient) { }

  getApiList(){
    return this.http.get(this.apiUrls);
  }
  loadApiData(URL){
    //var reqHeader = new HttpHeaders({ Authorization: "Bearer " + btoa(localStorage.access_token)})
    console.log('AccessToken', localStorage.access_token)
    var reqHeader = new HttpHeaders({ Authorization: "Bearer " + (localStorage.access_token)})
    return this.http.get(environment.host+'/'+environment.projectKey+'/'+URL, {headers: reqHeader});
  }
  loadData(data){
    this.details = data
  }

}


