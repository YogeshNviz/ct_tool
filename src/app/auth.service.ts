import { Injectable } from '@angular/core';
import { User } from './user';
import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest, HttpClient, HttpHeaders } from '@angular/common/http';
import { Router } from  '@angular/router';
import { ok } from 'assert';
//import { map } from 'rxjs/add/operator/map';


@Injectable({
  providedIn: 'root',
})
export class AuthService {

  //auth_url = 'https://auth.sphere.io/oauth/token?grant_type=client_credentials'
  auth_url = 'https://auth.europe-west1.gcp.commercetools.com/oauth/token?grant_type=client_credentials'
  users: User[] = [{ client_id: 'admin', client_secret: 'password'}];
 // client_id = 'QQtu3DqDTY1OkFQG19u6Bnkm'
 // client_secret = 'f4tvyOdSOcaWkkxOvpXHHUPMr_3c8ZTM'

  client_id = 'U92Sw7YgSs0SWJM8_HhcJpfj'
  client_secret = 'RoWgwVj4gxC_dwpWuzN6W6FOn0poddxw'


  constructor(private http: HttpClient, private router: Router) { }

  public login(userInfo: User){

    const user = this.users.find(x => x.client_id === userInfo.client_id && x.client_secret === userInfo.client_secret);
            if (!user) {
              return alert('Username or password is incorrect');
            }
            else{
              console.log(userInfo, this.auth_url)
                var userData = userInfo;
                var reqHeader = new HttpHeaders({ Authorization: "Basic " + btoa(this.client_id + ":" + this.client_secret)})

              //var reqHeader = new HttpHeaders({ 'Content-Type': 'application/x-www-form-urlencoded','No-Auth':'True' });
               // return this.http.post(this.auth_url, userData+'&scope=manage_project:nvizionproject',  { headers: reqHeader })
                return this.http.post(this.auth_url, userData+'&scope=nvizion-internal manage_products:nvizion-internal manage_order_edits:nvizion-internal manage_my_shopping_lists:nvizion-internal manage_customer_groups:nvizion-internal manage_my_profile:nvizion-internal manage_cart_discounts:nvizion-internal manage_my_orders:nvizion-internal view_api_clients:nvizion-internal manage_my_payments:nvizion-internal manage_categories:nvizion-internal manage_customers:nvizion-internal manage_import_sinks:nvizion-internal view_categories:nvizion-internal manage_api_clients:nvizion-internal manage_extensions:nvizion-internal manage_payments:nvizion-internal manage_discount_codes:nvizion-internal view_published_products:nvizion-internal create_anonymous_token:nvizion-internal manage_orders:nvizion-internal',  { headers: reqHeader })
                .subscribe(
                  result => {
                    console.log('Result',result);
                    localStorage.setItem('access_token', result['access_token']);
                    this.router.navigateByUrl('/dashboard');
                  },
                  err => {
                    console.log("Error- something is wrong!")
              });
            }
  }

  public isLoggedIn(){
    return localStorage.getItem('access_token') !== null;

  }

  public logout(){
    localStorage.removeItem('access_token');
  }
}